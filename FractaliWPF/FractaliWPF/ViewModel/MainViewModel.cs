﻿using GalaSoft.MvvmLight;
using System.Windows.Media.Media3D;
using FractaliWPF.Model;

namespace FractaliWPF.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        #region Properties

        private Planogram3DViewModel fractaPlanogram3DViewModel;
        public Planogram3DViewModel FractaPlanogram3DViewModel
        {
            get
            {
                if (fractaPlanogram3DViewModel == null)
                    fractaPlanogram3DViewModel = new Planogram3DViewModel();
                return fractaPlanogram3DViewModel;
            }
            set
            {
                fractaPlanogram3DViewModel = value;
            }
        }
        #endregion Properties

        public MainViewModel()
        {
        }
    }
}