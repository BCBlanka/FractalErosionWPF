﻿using GalaSoft.MvvmLight;
using System.Windows.Media.Media3D;
using FractaliWPF.Model;
using FractaliWPF.Model.Multithreading;
using System;
using GalaSoft.MvvmLight.Command;
using System.Threading;
using FractaliWPF.Model.Helpers;

namespace FractaliWPF.ViewModel
{

    public class Planogram3DViewModel : ViewModelBase
    {
        #region Variables
        DiamondSquareFractal fractal = new DiamondSquareFractal();
        float quantity = 0.1f;
        #endregion Variables

        #region Properties
        Model3DGroup model;
        public Model3DGroup Model
        {
            get
            {
                if (model == null)
                {
                    model = UpdateDiamondSquareFractalModel();
                }
                return model;
            }
            set
            {
                model = value;
                RaisePropertyChanged("Model");
            }
        }

        private int level = 5;
        public int Level
        {
            get { return level; }
            set
            {
                if (value == level) return;
                level = value;
                Model = UpdateDiamondSquareFractalModel();
                RaisePropertyChanged("Level");
            }
        }

        public int[] Levels
        {
            get
            {
                return new[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            }
        }

        private int gridSize = 7;
        public int GridSize
        {
            get { return gridSize; }
            set
            {
                if (value == gridSize) return;
                gridSize = value;
                Model = UpdateDiamondSquareFractalModel();
                RaisePropertyChanged("GridSize");
            }
        }

        public int[] GridSizes
        {
            get
            {
                return new[] { 2, 3, 4, 5, 6, 7, 8 };
            }
        }

        private int seed = 25;
        public int Seed
        {
            get { return seed; }
            set
            {
                if (value == seed) return;
                seed = value;
                Model = UpdateDiamondSquareFractalModel();
                RaisePropertyChanged("Seed");
            }
        }

        public int[] Seeds
        {
            get
            {
                return new[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 20, 25, 30, 35, 40, 45, 50, 60, 70, 80, 90, 100 };
            }
        }

        private int iterationThermal = 10;
        public int IterationThermal
        {
            get
            {
                return iterationThermal;
            }
            set
            {
                iterationThermal = value;
            }
        }

        private int iterationHidraulical = 10;
        public int IterationHidraulical
        {
            get
            {
                return iterationHidraulical;
            }
            set
            {
                iterationHidraulical = value;
            }
        }

        public float WaterQuantity
        {
            get
            {
                return quantity;
            }
            set
            {
                quantity = value;
            }
        }

        private bool isErosionButtonEnabled = true;
        public bool IsErosionButtonEnabled
        {
            get
            {
                return isErosionButtonEnabled;
            }
            set
            {
                isErosionButtonEnabled = value;
                RaisePropertyChanged("IsErosionButtonEnabled");
            }
        }
        #endregion Properties

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the Planogram3DViewModel class.
        /// </summary>
        public Planogram3DViewModel()
        {

        }
        #endregion Constructor

        
        #region Commands
        /// <summary>
        /// command Apply thermal Erosion
        /// </summary>
        public RelayCommand CommandApplyThermalErosion
        {
            get
            { return new RelayCommand(delegate { ApplyThermalErosion(); }); }
        }

        /// <summary>
        /// Apply thermal erosion to model
        /// </summary>
        private void ApplyThermalErosion()
        {
            BackgroundWorkerManager.RunTaskInBackground(delegate
            {
                BackgroundWorkerManager.RunTaskOnMainThread(new Action(delegate
                {
                    IsErosionButtonEnabled = false;
                }));
                for (int i = 0; i < IterationThermal; i++)
                {
                    BackgroundWorkerManager.RunTaskOnMainThread(new Action(delegate
                    {
                        Model = fractal.ApplyThermalErosion();
                    }));
                    Thread.Sleep(100);

                }

                BackgroundWorkerManager.RunTaskOnMainThread(new Action(delegate
                {
                    IsErosionButtonEnabled = true;
                }));
            });
        }

        /// <summary>
        /// Command apply hydraulic erosion
        /// </summary>
        public RelayCommand CommandApplyHydraulicErosion
        {
            get
            { return new RelayCommand(delegate { ApplyHydraulicErosion(); }); }
        }

        /// <summary>
        /// Apply hydraulic erosion to model
        /// </summary>
        private void ApplyHydraulicErosion()
        {
            Model3DGroup watermodel = Model.Clone();
            BackgroundWorkerManager.RunTaskInBackground(delegate
            {
                BackgroundWorkerManager.RunTaskOnMainThread(new Action(delegate
                {
                    IsErosionButtonEnabled = false;
                }));

                fractal.InitWaterquantity(quantity);
                for (int i = 0; i < IterationHidraulical; i++)
                {
                    BackgroundWorkerManager.RunTaskOnMainThread(new Action(delegate
                                    {
                                        fractal.InitWaterquantity(quantity);
                                        Model = fractal.ApplyHydraulicErosion(quantity);
                                    }));
                    Thread.Sleep(100);
                }
                BackgroundWorkerManager.RunTaskOnMainThread(new Action(delegate
                {
                    IsErosionButtonEnabled = true;
                }));

            });
        }

        #endregion Commands

        #region Methods
        /// <summary>
        /// Update the dianond-square fractal with the given input params
        /// </summary>
        /// <returns></returns>
        private Model3DGroup UpdateDiamondSquareFractalModel()
        {
            fractal.Roughness = Level;
            fractal.GridSize = (int)Math.Pow(2, GridSize) + 1;
            fractal.Seed = Seed;
            Model3DGroup _model = fractal.Generate();

            fractal.InitWaterquantity(quantity);
            return _model;
        }
        #endregion Methods

    }
}