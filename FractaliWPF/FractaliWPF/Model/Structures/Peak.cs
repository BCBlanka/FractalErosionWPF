﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FractaliWPF.Model.Structures
{
    public class Peak
    {
        public float Value { get; set; }
        public int X { get; set; }
        public int Y { get; set; }

        public Peak(float value, int x, int y) { Value = value; X = x; Y = y; }
    }
}
