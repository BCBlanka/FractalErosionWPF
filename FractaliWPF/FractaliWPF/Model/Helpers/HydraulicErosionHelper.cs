﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Media3D;

namespace FractaliWPF.Model.Helpers
{
    public static class HydraulicErosionHelper
    {
        public static float DelaT = 0.02f;
        public static float Kr = 0.012f;
        public static float A = 20f;
        public static float g = 9.81f;
        public static float Kc = 1f;
        public static float Kt = 0.15f;
        public static float Ks = 0.5f;
        public static float Kd = 1f;
        public static float Kh = 5f;
        public static float KdMax = 10f;
        public static float Ka = 0.8f;
        public static float Ki = 0.1f;

        public static Model3DGroup ApplyHidraulicErosion(float[][] grid)
        {
            Model3DGroup model = new Model3DGroup();

            return model;
        }
    }
}
