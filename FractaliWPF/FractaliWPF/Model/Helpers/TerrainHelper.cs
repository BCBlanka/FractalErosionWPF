﻿using GalaSoft.MvvmLight;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using System.Windows;

namespace FractaliWPF.Model
{
    public static class TerrainHelper 
    {
        /// <summary>
        /// Generate the texture coordinates for a given mesh
        /// </summary>
        /// <param name="mesh">the mesh</param>
        /// <returns>the texture coordinates</returns>
        public static PointCollection GenerateTextureCoordinates(MeshGeometry3D mesh)
        {
            PointCollection indices = new PointCollection();

            if (mesh.Positions.Count == 0)
                return indices;
            double minX = mesh.Positions[0].X;
            double maxX = mesh.Positions[0].X;
            double minY = mesh.Positions[0].Y;
            double maxY = mesh.Positions[0].Y;

            foreach (Point3D p in mesh.Positions)
            {
                if (p.X > maxX)
                    maxX = p.X;
                if (p.X < minX)
                    minX = p.X;
                if (p.Y > maxY)
                    maxY = p.Y;
                if (p.Y < minY)
                    minY = p.Y;
            }

            double factorX = maxX - minX;
            double offsetX = 0 - minX;

            maxY = 15;
            double factorY = maxY - minY;
            double offsetY = 0 - minY;
            foreach (Point3D p in mesh.Positions)
            {
                indices.Add(new Point((p.X + offsetX) / factorX, (p.Y + offsetY) / factorY));
            }
            return indices;
        }

        /// <summary>
        /// Generate texture coordinates for water
        /// </summary>
        /// <param name="mesh">the mesh</param>
        /// <param name="waterQuantity">the water quantity</param>
        /// <param name="GridSize">the size of the model</param>
        /// <returns>the texture coordinates for the water</returns>
        internal static PointCollection GenerateWaterTextureCoordinates(MeshGeometry3D mesh, float[][] waterQuantity, int GridSize)
        {
            PointCollection indices = new PointCollection();

            if (mesh.Positions.Count == 0)
                return indices;

            int indexY = 0;
            int indexX = 0;
            foreach (Point3D p in mesh.Positions)
            {
                indices.Add(new Point(waterQuantity[indexX][indexY], waterQuantity[indexX][indexY]));
                if(indexY == GridSize - 1)
                {
                    indexY = 0;
                    indexX++;
                }
                else
                {
                    indexY++;
                }
            }
            return indices;
        }
    }
}