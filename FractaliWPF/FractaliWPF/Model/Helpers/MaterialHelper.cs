﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Media3D;
using System.Windows.Media;
using System.Windows;

namespace FractaliWPF.Model.Helpers
{
    public static class MaterialHelper
    {
        /// <summary>
        /// Get the material for water
        /// </summary>
        /// <returns>the water material</returns>
        public static MaterialGroup GetWaterMaterial()
        {
            MaterialGroup group = new MaterialGroup();

            GradientStopCollection gradients = new GradientStopCollection();

            gradients.Add(new GradientStop(HelixToolkit.Wpf.ColorHelper.HexToColor("#002B65EC"), 0));
            gradients.Add(new GradientStop(HelixToolkit.Wpf.ColorHelper.HexToColor("#222B65EC"), 0.1));
            gradients.Add(new GradientStop(HelixToolkit.Wpf.ColorHelper.HexToColor("#332B65EC"), 0.2));
            gradients.Add(new GradientStop(HelixToolkit.Wpf.ColorHelper.HexToColor("#442B65EC"), 0.3));
            gradients.Add(new GradientStop(HelixToolkit.Wpf.ColorHelper.HexToColor("#552B65EC"), 0.4));
            gradients.Add(new GradientStop(HelixToolkit.Wpf.ColorHelper.HexToColor("#662B65EC"), 0.5));
            gradients.Add(new GradientStop(HelixToolkit.Wpf.ColorHelper.HexToColor("#772B65EC"), 0.6));
            gradients.Add(new GradientStop(HelixToolkit.Wpf.ColorHelper.HexToColor("#882B65EC"), 0.7));
            gradients.Add(new GradientStop(HelixToolkit.Wpf.ColorHelper.HexToColor("#992B65EC"), 0.8));
            gradients.Add(new GradientStop(HelixToolkit.Wpf.ColorHelper.HexToColor("#AA2B65EC"), 0.9));
            gradients.Add(new GradientStop(HelixToolkit.Wpf.ColorHelper.HexToColor("#BB2B65EC"), 1.0));

            GradientBrush gradient = new LinearGradientBrush(gradients, new Point(0, 0), new Point(1, 1));
            gradient.MappingMode = BrushMappingMode.RelativeToBoundingBox;

            group.Children.Add(new DiffuseMaterial(gradient) { AmbientColor = Colors.LightBlue });
            group.Children.Add(new SpecularMaterial(new SolidColorBrush(Colors.LightBlue), 15));
            return group;
        }

        /// <summary>
        /// Get the terrain material
        /// </summary>
        /// <returns>the material for the terrain</returns>
        public static MaterialGroup GetTerrainMaterial()
        {
            MaterialGroup group = new MaterialGroup();

            GradientStopCollection gradients = new GradientStopCollection();

            gradients.Add(new GradientStop(HelixToolkit.Wpf.ColorHelper.HexToColor("#00FA9A"), 0));
            gradients.Add(new GradientStop(HelixToolkit.Wpf.ColorHelper.HexToColor("#00FF66"), 0.1));
            gradients.Add(new GradientStop(HelixToolkit.Wpf.ColorHelper.HexToColor("#00FF33"), 0.2));
            gradients.Add(new GradientStop(Colors.Green, 0.3));
            gradients.Add(new GradientStop(Colors.Green, 0.4));
            gradients.Add(new GradientStop(Colors.Green, 0.5));
            gradients.Add(new GradientStop(HelixToolkit.Wpf.ColorHelper.HexToColor("#D1E231"), 0.6));
            gradients.Add(new GradientStop(HelixToolkit.Wpf.ColorHelper.HexToColor("#CDC673"), 0.7));
            gradients.Add(new GradientStop(HelixToolkit.Wpf.ColorHelper.HexToColor("#8B6914"), 0.8));
            gradients.Add(new GradientStop(HelixToolkit.Wpf.ColorHelper.HexToColor("#8B4500"), 0.9));
            gradients.Add(new GradientStop(HelixToolkit.Wpf.ColorHelper.HexToColor("#5E2605"), 1.0));

            GradientBrush gradient = new LinearGradientBrush(gradients, new Point(0, 0), new Point(0, 1));
            gradient.MappingMode = BrushMappingMode.Absolute;

            group.Children.Add(new SpecularMaterial(new SolidColorBrush(Colors.White), 10));
            group.Children.Add(new DiffuseMaterial(gradient) { AmbientColor = Colors.Orange });
            
            return group;
        }
    }
}
