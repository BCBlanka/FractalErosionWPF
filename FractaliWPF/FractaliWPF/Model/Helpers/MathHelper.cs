﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FractaliWPF.Model.Helpers
{
    public static class MathHelper
    {
        /// <summary>
        /// Gets a random number in the specified rangle
        /// </summary>
        /// <param name="r">the random object</param>
        /// <param name="rMin">the rangle lower limit</param>
        /// <param name="rMax">the range upper limit</param>
        /// <returns>the random number</returns>
        public static int RandRange(Random r, int rMin, int rMax)
        {
            return rMin + r.Next() * (rMax - rMin);
        }

        /// <summary>
        /// Gets a random number in the specified rangle
        /// </summary>
        /// <param name="r">the random object</param>
        /// <param name="rMin">the rangle lower limit</param>
        /// <param name="rMax">the range upper limit</param>
        /// <returns>the random number</returns>
        public static double RandRange(Random r, double rMin, double rMax)
        {
            return rMin + r.NextDouble() * (rMax - rMin);
        }

        /// <summary>
        /// Gets a random number in the specified rangle
        /// </summary>
        /// <param name="r">the random object</param>
        /// <param name="rMin">the rangle lower limit</param>
        /// <param name="rMax">the range upper limit</param>
        /// <returns>the random number</returns>
        public static float RandRange(Random r, float rMin, float rMax)
        {
            return rMin + (float)r.NextDouble() * (rMax - rMin);
        }

        /// <summary>
        /// Checks if a number is a power of 2
        /// </summary>
        /// <param name="a">the number</param>
        /// <returns>true if the value is a power of 2, otherwise false</returns>
        public static bool Pow2(int a)
        {
            return (a & (a - 1)) == 0;
        }
    }
}
