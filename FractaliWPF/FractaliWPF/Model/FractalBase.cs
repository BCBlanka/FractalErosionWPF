﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows.Media.Media3D;

namespace FractaliWPF.Model
{
    public abstract class FractalBase
    {
        public int Level { get; set; }
        public abstract Model3DGroup Generate();
    }
}
