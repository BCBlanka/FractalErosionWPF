﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows;

namespace FractaliWPF.Model.Multithreading
{
    public class BackgroundWorkerManager
    {
        /// <summary>
        /// Runs the task in async mode
        /// </summary>
        /// <param name="work">the work task</param>
        /// <param name="asyncArgument">the argument to be passed to the async delegate</param>
        /// <param name="messageTaskStart">the message displayed on the status bar when the task is started</param>
        /// <param name="doResetStatusBar">if true, the status bar text is resetted at the completion of the task</param>
        public static BackgroundWorker RunTaskInBackground(DoWorkEventHandler work, object asyncArgument, string messageTaskStart, bool doResetStatusBar)
        {
            // if we are already running on a worker thread we will just run the task
            if (Application.Current.Dispatcher.Thread.ManagedThreadId != System.Threading.Thread.CurrentThread.ManagedThreadId)
            {
                work.Invoke(null, null);
                return null;
            }
            BackgroundWorker worker = new BackgroundWorker();
            worker.WorkerSupportsCancellation = true;
            
            worker.DoWork += work;
                        
            // start the task in async mode
            worker.RunWorkerAsync(asyncArgument);

            return worker;

        }

        /// <summary>
        /// Runs the task in async mode
        /// </summary>
        /// <param name="work">the work task</param>
        public static BackgroundWorker RunTaskInBackground(DoWorkEventHandler work)
        {
            return RunTaskInBackground(work, null, "", false);
        }

        /// <summary>
        /// Runt the given Action on the main thread
        /// </summary>
        /// <param name="task">the task to be completed</param>
        /// <param name="args">the arguments for the Action</param>
        public static void RunTaskOnMainThread(Action task, params object[] args)
        {
            Application.Current.Dispatcher.Invoke(task, args);
        }
    }
}
