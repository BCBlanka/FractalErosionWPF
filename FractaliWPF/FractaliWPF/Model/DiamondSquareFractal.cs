﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Media3D;
using System.Windows.Media;
using System.Windows;
using FractaliWPF.Model.Helpers;
using FractaliWPF.Model.Structures;
using FractaliWPF.Model.Multithreading;

namespace FractaliWPF.Model
{
    public class DiamondSquareFractal : FractalBase
    {
        #region Variables
        public int Roughness = 15;
        public int GridSize = 129;
        public int Seed = 50;
        public float sedimentFactor = 0.5f; 
        public float Kd = 0.1f;
        public float Kc = 5f;
        public float Ks = 0.3f;
        private float[][] ds;
        private float[][] waterQuantity;
        private float[][] sedimentQuantity;
        private bool[][] dsChecked;
        #endregion Variables

        #region Diamond-Square
        /// <summary>
        /// Generate the terrain
        /// </summary>
        /// <returns>the 3D model</returns>
        public override Model3DGroup Generate()
        {
            ds = DiamondSquareGrid(GridSize, Seed, 0, 40, Roughness / 5.0f);
            return GenerateTerrain();
        }

        /// <summary>
        /// Generate the terrain
        /// </summary>
        /// <param name="isWaterMap">is the terrain a water terrain</param>
        /// <returns>the 3D model</returns>
        private Model3DGroup GenerateTerrain(bool isWaterMap = false)
        {
            if (ds == null)
                return new Model3DGroup();
            
            Vector3DCollection verts = new Vector3DCollection(GridSize * GridSize);
            Point3DCollection points = new Point3DCollection(GridSize * GridSize);
            Int32Collection indices = new Int32Collection();
            for (int y = 0; y < GridSize; y++)
            {
                for (int x = 0; x < GridSize; x++)
                {
                    if (isWaterMap)
                    {
                        verts.Insert(x + y * GridSize, new Vector3D((x - (GridSize / 2)) * 0.25f,
                                                            ((ds[x][y] + waterQuantity[x][y]) / 2) * 1.0f,
                                                        (y - (GridSize / 2)) * 0.25f));
                        points.Insert(x + y * GridSize, new Point3D((x - (GridSize / 2)) * 0.25f,
                                                            ((ds[x][y] + waterQuantity[x][y]) / 2) * 1.0f,
                                                        (y - (GridSize / 2)) * 0.25f));
                    }
                    else
                    {
                        verts.Insert(x + y * GridSize, new Vector3D((x - (GridSize / 2)) * 0.25f,
                                                            (ds[x][y] / 2) * 1.0f,
                                                        (y - (GridSize / 2)) * 0.25f));
                        points.Insert(x + y * GridSize, new Point3D((x - (GridSize / 2)) * 0.25f,
                                                            (ds[x][y] / 2) * 1.0f,
                                                        (y - (GridSize / 2)) * 0.25f));
                    }
                }
            }

            int ctr = 0;
            indices = new Int32Collection((GridSize - 1) * (GridSize - 1) * 6);
            
            for (int y = 0; y < GridSize - 1; y++)
            {
                for (int x = 0; x < GridSize - 1; x++)
                {
                    // tl - - tr
                    //  | \   |
                    //  |   \ |
                    // bl - - br

                    int tl = x + (y) * GridSize;
                    int tr = (x + 1) + (y) * GridSize;
                    int bl = x + (y + 1) * GridSize;
                    int br = (x + 1) + (y + 1) * GridSize;

                    // indices for first triangle
                    indices.Insert(ctr++,(int)tl);
                    indices.Insert(ctr++,(int)br);
                    indices.Insert(ctr++, (int)bl);

                    // indices for 2nd triangle
                    indices.Insert(ctr++,(int)tl);
                    indices.Insert(ctr++,(int)tr);
                    indices.Insert(ctr++,(int)br);
                }
            }
            

            MeshGeometry3D mesh = new MeshGeometry3D();
            mesh.Positions = points;
            mesh.Normals = verts;
            mesh.TriangleIndices = indices;
            if (isWaterMap)
            {
                mesh.TextureCoordinates = TerrainHelper.GenerateWaterTextureCoordinates(mesh, waterQuantity, GridSize);
            }
            else
            {
                mesh.TextureCoordinates = TerrainHelper.GenerateTextureCoordinates(mesh);
            }
            GeometryModel3D geo = new GeometryModel3D();
            MaterialGroup mat = MaterialHelper.GetTerrainMaterial();
            if (isWaterMap)
            {
                mat = MaterialHelper.GetWaterMaterial();
            }
            
            geo.Geometry = mesh;
            geo.Material = mat;
            geo.BackMaterial = mat;

            Model3DGroup model = new Model3DGroup();
            model.Children.Add(geo);

            return model;
        }

        /// <summary>
        /// Generate the diamond-square height map
        /// </summary>
        /// <param name="size">the size of the grid</param>
        /// <param name="seed">the Random object init param</param>
        /// <param name="rMin">random min value</param>
        /// <param name="rMax">random max value</param>
        /// <param name="noise">noise</param>
        /// <returns>the 3D model</returns>
        private float[][] DiamondSquareGrid(int size, int seed = 0, float rMin = 0, float rMax = 255, float noise = 0.0f)
        {
            // Fail if grid size is not of the form (2 ^ n) - 1 or if min/max values are invalid
            int s = size - 1;
            if (!MathHelper.Pow2(s) || rMin >= rMax)
                return null;

            float modNoise = 0.0f;

            // init the grid
            float[][] grid = new float[size][];
            for (int i = 0; i < size; i++)
                grid[i] = new float[size];

            // Seed the first four corners
            Random rand = (seed == 0 ? new Random() : new Random(seed));
            grid[0][0] = 0;
            grid[s][0] = 0;
            grid[0][s] = 0;
            grid[s][s] = 0;

            /*
             * Use temporary named variables to simplify equations
             * 
             * s0 . d0. s1
             *  . . . . . 
             * d1 . cn. d2
             *  . . . . . 
             * s2 . d3. s3
             * 
             * */
            float s0, s1, s2, s3, d0, d1, d2, d3, cn;

            for (int i = s; i > 1; i /= 2)
            {
                // reduce the random range at each step
                modNoise = (rMax - rMin) * noise * ((float)i / s);

                // diamonds
                for (int y = 0; y < s; y += i)
                {
                    for (int x = 0; x < s; x += i)
                    {
                        s0 = grid[x][y];
                        s1 = grid[x + i][y];
                        s2 = grid[x][y + i];
                        s3 = grid[x + i][y + i];

                        // cn
                        grid[x + (i / 2)][y + (i / 2)] = ((s0 + s1 + s2 + s3) / 4.0f)
                            + MathHelper.RandRange(rand, -modNoise, modNoise);
                    }
                }

                // squares
                for (int y = 0; y < s; y += i)
                {
                    for (int x = 0; x < s; x += i)
                    {
                        s0 = grid[x][y];
                        s1 = grid[x + i][y];
                        s2 = grid[x][y + i];
                        s3 = grid[x + i][y + i];
                        cn = grid[x + (i / 2)][y + (i / 2)];

                        d0 = y <= 0 ? (s0 + s1 + cn) / 3.0f : (s0 + s1 + cn + grid[x + (i / 2)][y - (i / 2)]) / 4.0f;
                        d1 = x <= 0 ? (s0 + cn + s2) / 3.0f : (s0 + cn + s2 + grid[x - (i / 2)][y + (i / 2)]) / 4.0f;
                        d2 = x >= s - i ? (s1 + cn + s3) / 3.0f :
                            (s1 + cn + s3 + grid[x + i + (i / 2)][y + (i / 2)]) / 4.0f;
                        d3 = y >= s - i ? (cn + s2 + s3) / 3.0f :
                            (cn + s2 + s3 + grid[x + (i / 2)][y + i + (i / 2)]) / 4.0f;

                        grid[x + (i / 2)][y] = d0 + MathHelper.RandRange(rand, -modNoise, modNoise);
                        grid[x][y + (i / 2)] = d1 + MathHelper.RandRange(rand, -modNoise, modNoise);
                        grid[x + i][y + (i / 2)] = d2 + MathHelper.RandRange(rand, -modNoise, modNoise);
                        grid[x + (i / 2)][y + i] = d3 + MathHelper.RandRange(rand, -modNoise, modNoise);
                    }
                }
            }

            return grid;
        }
        #endregion Diamond-Square

        #region Thermal Erosion
        /// <summary>
        /// Apply thermal erosion to the model
        /// </summary>
        /// <returns>the model</returns>
        public Model3DGroup ApplyThermalErosion()
        {
            ApplyThermalErosionToSquare();
            return GenerateTerrain();
        }

        /// <summary>
        /// Apply thermal erosion to diamond-square grid
        /// </summary>
        private void ApplyThermalErosionToSquare()
        {
            if (ds == null)
                return;

            float erosionHeight = 0.01f;
            dsChecked = new bool[GridSize][];
            for (int i = 0; i < GridSize; i++)
            {
                dsChecked[i] = new bool[GridSize];
                for (int j = 0; j < GridSize; j++)
                {
                    dsChecked[i][j] = false;
                }
            }

            float peak = ds[0][0];
            float low = ds[0][0];
            int posX = 0, posY = 0;
            /// look for the highest point
            for (int i = 0; i < GridSize; i++)
            {
                for (int j = 0; j < GridSize; j++)
                {
                    if(ds[i][j] > peak)
                    {
                        peak = ds[i][j];
                        posX = i;
                        posY = j;
                    }
                    if (ds[i][j] < low)
                    {
                        low = ds[i][j];
                    }
                }
            }
            erosionHeight = (peak - low) / 1000;
            ApplyThermalErosionToNeighbours(erosionHeight, posX, posY);
        }

        /// <summary>
        /// Apply thermal erosion from peak to neighbours
        /// </summary>
        /// <param name="erosionHeight">erosion quantity</param>
        /// <param name="posX">peak posX</param>
        /// <param name="posY">peak posY</param>
        private void ApplyThermalErosionToNeighbours(float erosionHeight, int posX, int posY)
        {
            for (int x = 0; x < GridSize; x++)
            {
                for (int y = 0; y < GridSize; y++)
                {
                    for (int i = -1; i <= 1; i++)
                    {
                        posX = x;
                        posY = y;
                        for (int j = -1; j <= 1; j++)
                        {
                            // check if neighbour is inside the matrix
                            if (posX + i >= 0 &&
                                posX + i < GridSize &&
                                posY + j >= 0 &&
                                posY + j < GridSize)
                            {
                                // check if material needs to be moved to the neighbour
                                if (ds[posX + i][posY + j] < ds[posX][posY])
                                {
                                    ds[posX + i][posY + j] += erosionHeight;
                                    ds[posX][posY] -= erosionHeight;

                                }
                            }
                        }
                    }
                }
            }
        }
        #endregion Thermal Erosion

        #region Hydaraulic erosion
        /// <summary>
        /// Apply Hydraulic erosion 
        /// </summary>
        /// <param name="waterQuantityX">water quantity</param>
        /// <returns>the eroded model</returns>
        internal Model3DGroup ApplyHydraulicErosion(float waterQuantityX)
        {
            Model3DGroup model = new Model3DGroup();
            if (ds == null)
                return model;
            List<Peak> peaks = new List<Peak>();
            dsChecked = new bool[GridSize][];
            for (int i = 0; i < GridSize; i++)
            {
                dsChecked[i] = new bool[GridSize];
                for (int j = 0; j < GridSize; j++)
                {
                    dsChecked[i][j] = false;
                }
            }

            for (int x = 0; x < GridSize; x++)
            {
                for (int y = 0; y < GridSize; y++)
                {
                    if(IsPeak(ds[x][y], x, y))
                    {
                        peaks.Add(new Peak(ds[x][y], x, y));
                    }
                }
            }

            float maxWaterQuantity = 10 * waterQuantityX;
            foreach (Peak p in peaks)
            {
                ApplyHydraulicErosionFromPeak(p);
            }
            RemoveExcessWaterFromMargins();
            return GenerateTerrain();
        }

        /// <summary>
        /// Apply hydraulic erosion frpn Peak
        /// </summary>
        /// <param name="p"></param>
        private void ApplyHydraulicErosionFromPeak(Peak p)
        {
            dsChecked[p.X][p.Y] = true;

            float height = ds[p.X][p.Y];
            int neighbourcount = 0;
            for (int i = -1; i <= 1; i++)
            {
                for (int j = -1; j <= 1; j++)
                {
                    if (p.X + i >= 0 &&
                        p.X + i < GridSize &&
                        p.Y + j >= 0 &&
                        p.Y + j < GridSize)
                    {
                        if (ds[p.X + i][p.Y + j] < ds[p.X][p.Y])
                            neighbourcount++;
                    }
                }
            }

            if (neighbourcount == 0)
                return;
            for (int i = -1; i <= 1; i++)
            {
                for (int j = -1; j <= 1; j++)
                {
                    if (p.X + i >= 0 &&
                        p.X + i < GridSize &&
                        p.Y + j >= 0 &&
                        p.Y + j < GridSize)
                    {
                        if (!(i == 0 || j == 0))
                            continue;
                        // we do this only if the peak is higher
                        if (ds[p.X][p.Y] > ds[p.X + i][p.Y + j])
                        {
                            
                                bool bIsMargin = false;
                                if (p.X + i == 0 ||
                                    p.X + i == GridSize ||
                                    p.Y + j == 0 ||
                                    p.Y + j == GridSize)
                                {
                                    bIsMargin = true;
                                }
                                if (!bIsMargin)
                                {
                                    // 1. Move water from the peak to the neighbours
                                    height = ds[p.X][p.Y];
                                    float neighbourHeight = ds[p.X + i][p.Y + j];
                                    float wt = MoveWater(height, neighbourHeight, waterQuantity[p.X][p.Y], waterQuantity[p.X + i][p.Y + j]);

                                    //if (height - wt > neighbourHeight + wt)
                                    //{

                                    if (wt > 0)
                                    {
                                        waterQuantity[p.X][p.Y] -= wt;
                                        waterQuantity[p.X + i][p.Y + j] += wt;

                                        // 2. Move sediment to the neighbours
                                        ds[p.X][p.Y] -= wt * sedimentFactor;
                                        ds[p.X + i][p.Y + j] += wt * sedimentFactor;

                                        float cs = Kc * wt;

                                        if (sedimentQuantity[p.X][p.Y] >= cs)
                                        {
                                            sedimentQuantity[p.X + i][p.Y + j] += cs;
                                            ds[p.X][p.Y] += Kd * (sedimentQuantity[p.X][p.Y] - cs);
                                            sedimentQuantity[p.X][p.Y] = (1 - Kd) * (sedimentQuantity[p.X][p.Y] - cs);
                                        }
                                        else
                                        {
                                            sedimentQuantity[p.X + i][p.Y + j] += sedimentQuantity[p.X][p.Y] + Ks * (cs - sedimentQuantity[p.X][p.Y]);
                                            ds[p.X][p.Y] += -Ks * (cs - sedimentQuantity[p.X][p.Y]);
                                            sedimentQuantity[p.X][p.Y] = 0;
                                        }
                                    }
                                    else
                                    {
                                        ds[p.X][p.Y] += Ks * sedimentQuantity[p.X][p.Y];
                                    }
                                }
                                else
                                {
                                    waterQuantity[p.X + i][p.Y + j] = 0;
                                    sedimentQuantity[p.X + i][p.Y + j] = 0;
                                }
                                if (!dsChecked[p.X + i][p.Y + j])
                                    ApplyHydraulicErosionFromPeak(new Peak(ds[p.X + i][p.Y + j], p.X + i, p.Y + j));
                            
                        }
                    }
                }
            }

        }

        /// <summary>
        /// Initialize the terrain with a given amout of water quantity
        /// </summary>
        /// <param name="qnt">the quantity</param>
        internal void InitWaterquantity(float qnt)
        {
            waterQuantity = new float[GridSize][];
            sedimentQuantity = new float[GridSize][];
            for (int i = 0; i < GridSize; i++)
            {
                waterQuantity[i] = new float[GridSize];
                sedimentQuantity[i] = new float[GridSize];
                for (int j = 0; j < GridSize; j++)
                {
                    waterQuantity[i][j] = qnt;
                    sedimentQuantity[i][j] = qnt * sedimentFactor;
                }
            }
        }
        #endregion Hydaraulic erosion

        #region Helper Methods
        /// <summary>
        /// Calculate the water quantity which will be transported to the neighbour
        /// </summary>
        /// <param name="height">peak height</param>
        /// <param name="neighbourHeight">neighbour height</param>
        /// <param name="waterQ">water quantity</param>
        /// <param name="waterQNeighbour">neighbour water quantity</param>
        /// <returns>the water quantity that needs to be transported</returns>
        private float MoveWater(float height, float neighbourHeight, float waterQ, float waterQNeighbour)
        {
            float min = Math.Min(waterQ, (waterQ + height) - (waterQNeighbour + neighbourHeight));
            return min;
        }

        /// <summary>
        /// Remove pools of water (simulate evaporation)
        /// </summary>
        private void RemoveExcessWaterFromMargins()
        {
            List<Peak> lowPeaks = new List<Peak>();
            for (int x = 0; x < GridSize; x++)
            {
                for (int y = 0; y < GridSize; y++)
                {
                    if (IsPeak(ds[x][y], x, y, false))
                    {
                        lowPeaks.Add(new Peak(ds[x][y], x, y));
                    }
                }
            }

            foreach (Peak p in lowPeaks)
            {
                waterQuantity[p.X][p.Y] = 0f;
            }
        }

        /// <summary>
        /// Check if a given point is a peak
        /// </summary>
        /// <param name="value"></param>
        /// <param name="X"></param>
        /// <param name="Y"></param>
        /// <param name="upper"></param>
        /// <returns></returns>
        private bool IsPeak(float value, int X, int Y, bool upper = true)
        {
            bool isPeak = true;

            for (int i = -1; i <= 1; i++)
            {
                for (int j = -1; j <= 1; j++)
                {
                    // check if neighbour is inside the matrix
                    if (X + i >= 0 &&
                        X + i < GridSize &&
                        Y + j >= 0 &&
                        Y + j < GridSize)
                    {
                        if (upper)
                        {
                            if (ds[X + i][Y + j] > value)
                            {
                                isPeak = false;
                                continue;
                            }
                        }
                        else
                        {
                            if (ds[X + i][Y + j] < value)
                            {
                                isPeak = false;
                                continue;
                            }
                        }
                    }
                }
                if (!isPeak)
                {
                    continue;
                }
            }
            return isPeak;
        }
        #endregion Helper Methods
    }
}
