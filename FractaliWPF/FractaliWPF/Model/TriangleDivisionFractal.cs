﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Media3D;
using System.Windows.Media;
using FractaliWPF.Model.Helpers;

namespace FractaliWPF.Model
{
    public struct Triangle
    {
        public Point3D P1;
        public Point3D P2;
        public Point3D P3;
    }

    public class TriangleDivisionFractal : FractalBase
    {
        #region Properties
        private double width = 300;
        public double Width
        {
            get
            { return width; }
            set
            { width = value; }
        }

        private double height = 300;
        public double Height
        {
            get
            { return height; }
            set
            { height = value; }
        }

        private double depth = 300;
        public double Depth
        {
            get
            { return depth; }
            set
            { depth = value; }
        }

        public int MaxIterations = 3;
        Random r = new Random();
        #endregion Properties

        public override Model3DGroup Generate()
        {
            Model3DGroup terrain = new Model3DGroup();
            MeshGeometry3D meshTerrain = new MeshGeometry3D();

            Point3DCollection points = new Point3DCollection();
            Vector3DCollection vectors = new Vector3DCollection();
            Int32Collection triangleindices = new Int32Collection();

            Point3D x0 = new Point3D(0, Random(Height), 0);
            Point3D x1 = new Point3D(0, Random(Height), Depth);
            Point3D x2 = new Point3D(Width, Random(Height), 0);
            Point3D x3 = new Point3D(Width, Random(Height), Depth);

            terrain.Children.Clear();

            GenerateTerrain(new Triangle() { P1 = x0, P2 = x1, P3 = x2 }, ref terrain);
            GenerateTerrain(new Triangle() { P1 = x1, P2 = x2, P3 = x3 }, ref terrain);

            return terrain;
        }

        private void GenerateTerrain(Triangle first, 
            ref Model3DGroup terrain)
        {
            List<Triangle> triangles = new List<Triangle>();
            triangles.Add(first);
            for (int i = 0; i < MaxIterations; i++)
            {
                List<Triangle> dividedTriangles = new List<Triangle>();
                foreach (Triangle t in triangles)
                {
                    DivideTriangle(t, ref dividedTriangles);
                }
                triangles = dividedTriangles;
                DrawTriangles(terrain, triangles);
            }

            DrawTriangles(terrain, triangles);

        }

        private void ProcessTriangles(Model3DGroup terrain, List<Triangle> triangles)
        {
            MeshGeometry3D mesh = new MeshGeometry3D();
            foreach (Triangle t in triangles)
            {
                //Point3D highest = GetMaxHeightPoint(t);
                //mesh.Positions.Add(highest);
                //mesh.Normals.Add(new Vector3D(highest.X, highest.Y, highest.Z));

                mesh.Positions.Add(t.P1);
                mesh.Normals.Add(new Vector3D(t.P1.X, t.P1.Y, t.P1.Z));
                mesh.Positions.Add(t.P2);
                mesh.Normals.Add(new Vector3D(t.P2.X, t.P2.Y, t.P2.Z));
                mesh.Positions.Add(t.P3);
                mesh.Normals.Add(new Vector3D(t.P3.X, t.P3.Y, t.P3.Z));
            }
            for (int i = 0; i < mesh.Positions.Count - 2; i++)
            {
                mesh.TriangleIndices.Add(i);
                mesh.TriangleIndices.Add(i + 1);
                mesh.TriangleIndices.Add(i + 2);
            }

            mesh.TextureCoordinates = TerrainHelper.GenerateTextureCoordinates(mesh);

            GeometryModel3D geo = new GeometryModel3D();
            MaterialGroup mat = MaterialHelper.GetTerrainMaterial();
            geo.Geometry = mesh;
            geo.Material = mat;
            geo.BackMaterial = mat;

            terrain.Children.Add(geo);
        }

        private Point3D GetMaxHeightPoint(Triangle t)
        {
            return t.P1.Y > t.P2.Y ? t.P1.Y > t.P3.Y ? t.P1 : t.P2.Y > t.P3.Y ? t.P2 : t.P3 : t.P2.Y > t.P3.Y ? t.P2 : t.P3;
        }

        private void DrawTriangles(Model3DGroup terrain, List<Triangle> triangles)
        {
            foreach (Triangle t in triangles)
            {
                Point3DCollection points = new Point3DCollection();
                points.Add(t.P1);
                points.Add(t.P2);
                points.Add(t.P3);

                Vector3DCollection vectors = new Vector3DCollection();
                vectors.Add(new Vector3D(t.P1.X, t.P1.Y, t.P1.Z));
                vectors.Add(new Vector3D(t.P2.X, t.P2.Y, t.P2.Z));
                vectors.Add(new Vector3D(t.P3.X, t.P3.Y, t.P3.Z));

                Int32Collection triangleindices = new Int32Collection();
                triangleindices.Add(0);
                triangleindices.Add(1);
                triangleindices.Add(2);

                MeshGeometry3D meshTerrain = new MeshGeometry3D();
                meshTerrain.Positions = points;
                meshTerrain.Normals = vectors;
                meshTerrain.TriangleIndices = triangleindices;

                GeometryModel3D geo = new GeometryModel3D();
                geo.Geometry = meshTerrain;
                Color c = RandomColor();
                geo.Material = new DiffuseMaterial(new SolidColorBrush(c));
                geo.BackMaterial = new DiffuseMaterial(new SolidColorBrush(c));

                terrain.Children.Add(geo);
            }
        }

        #region Methods
        private void DivideTriangle(Triangle t, ref List<Triangle> dividedTriangles)
        {

            //Point3D midPoint1 = new Point3D((t.P1.X + t.P2.X) / 2, Random(t.P1.Y, t.P2.Y), (t.P1.Z + t.P2.Z) / 2);
            //Point3D midPoint2 = new Point3D((t.P3.X + t.P3.X) / 2, Random(t.P1.Y, t.P2.Y), (t.P2.Z + t.P3.Z) / 2);
            //Point3D midPoint3 = new Point3D((t.P3.X + t.P1.X) / 2, Random(t.P1.Y, t.P2.Y), (t.P3.Z + t.P1.Z) / 2);

            //Point3D midPoint1 = new Point3D((t.P1.X + t.P2.X) / 2, (2 * Random(t.P1.Y, t.P2.Y)) / 2, (t.P1.Z + t.P2.Z) / 2);
            //Point3D midPoint2 = new Point3D((t.P2.X + t.P3.X) / 2, 2 * Random(t.P2.Y, t.P3.Y) / 2, (t.P2.Z + t.P3.Z) / 2);
            //Point3D midPoint3 = new Point3D((t.P3.X + t.P1.X) / 2, 2 * Random(t.P3.Y, t.P1.Y) / 2, (t.P3.Z + t.P1.Z) / 2);

            Point3D midPoint1 = new Point3D((t.P1.X + t.P2.X) / 2, (t.P1.X + t.P2.X) / 2 * Random(0,2), (t.P1.Z + t.P2.Z) / 2);
            Point3D midPoint2 = new Point3D((t.P2.X + t.P3.X) / 2, (t.P3.X + t.P3.X) / 2 * Random(0, 2), (t.P2.Z + t.P3.Z) / 2);
            Point3D midPoint3 = new Point3D((t.P3.X + t.P1.X) / 2, (t.P3.X + t.P1.X) / 2 * Random(0, 2), (t.P3.Z + t.P1.Z) / 2);

            dividedTriangles.Add(new Triangle() { P1 = t.P1, P2 = midPoint1, P3 = midPoint3 });
            dividedTriangles.Add(new Triangle() { P1 = t.P2, P2 = midPoint1, P3 = midPoint2 });
            dividedTriangles.Add(new Triangle() { P1 = t.P3, P2 = midPoint2, P3 = midPoint3 });
            dividedTriangles.Add(new Triangle() { P1 = midPoint1, P2 = midPoint2, P3 = midPoint3 });
        }
        private double Random(double maxValue)
        {
            return r.NextDouble() * maxValue;
        }

        private double Random(double minValue, double maxValue)
        {
            if (minValue > maxValue)
            {
                double tmp = minValue;
                minValue = maxValue;
                maxValue = tmp;
            }
            return r.NextDouble() * (maxValue - minValue) + minValue;
        }

        private Color RandomColor()
        {
            return Color.FromRgb((byte)(r.NextDouble() * 255), (byte)(r.NextDouble() * 255), (byte)(r.NextDouble() * 255));
        }
        #endregion Methods
    }
}
