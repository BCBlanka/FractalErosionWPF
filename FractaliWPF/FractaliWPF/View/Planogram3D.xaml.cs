﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace FractaliWPF.View
{
    /// <summary>
    /// Description for Planogram3D.
    /// </summary>
    public partial class Planogram3D : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the Planogram3D class.
        /// </summary>
        public Planogram3D()
        {
            InitializeComponent();
            Init3DPlanogramm();
        }

        private void Init3DPlanogramm()
        {
            FractalViewport3D.RotateGesture = new System.Windows.Input.MouseGesture(MouseAction.LeftClick);
            FractalViewport3D.PanGesture = new System.Windows.Input.MouseGesture(MouseAction.RightClick);
        }
    }
}